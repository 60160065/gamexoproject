import java.util.Scanner;

public class GameXO {
	static Scanner kb = new Scanner(System.in);
	static char[][]board= {
			{' ','1','2','3'},
			{'1','-','-','-'},
			{'2','-','-','-'},
			{'3','-','-','-'}
	};
	static int check = 0,Break = 0,turn = 1,n1,n2;
	static char player = 'X';
	
	public static void main(String[] args) {
		printWelcome();
		while (true) {
			printBoard();
			input();
			chackWin();
			if(Break==1) {
				break;
			}
			SwitchTurn();
		}		
		printBoard();
		printWin();

	}
	
	static void printWelcome() {
		System.out.println("Welcome to OX Game.");
	}
	
	static void printBoard() {
		for(int row = 0;row<board.length;row++) {
			for(int col = 0;col<board[row].length;col++) {
				System.out.print(board[row][col]+" ");
			}System.out.println();
		}
	}
	
	static void input() {
		while (true) {
			System.out.print(player +" (R,C): ");
			n1=kb.nextInt();
			n2=kb.nextInt();
			if(board[n1][n2]=='-') {
				board[n1][n2]=player;
				break;
			}else {
				System.out.println("input mistakes, Please try again.");
			}
		}
	}
	
	static void chackWin() {
		HorizontalWin();
		VerticalWin();
		DiagonalWin();
		Draw();
	}
	
	static void HorizontalWin() {
		if((board[1][1]==board[1][2]&&board[1][1]==board[1][3]&&board[1][1]!='-')||
			(board[2][1]==board[2][2]&&board[2][1]==board[2][3]&&board[2][1]!='-')||
			(board[3][1]==board[3][2]&&board[1][1]==board[3][3]&&board[3][1]!='-')){
			if(player=='X') {
				check=1;
			}else {
				check=2;
			}
			Break=1;
		}
	}
	
	static void VerticalWin() {
		if((board[1][1]==board[2][1]&&board[1][1]==board[3][1]&&board[1][1]!='-')||  
			(board[1][2]==board[2][2]&&board[1][2]==board[3][2]&&board[1][2]!='-')||
			(board[1][3]==board[2][3]&&board[1][3]==board[3][3]&&board[1][3]!='-')){
			if(player=='X') {
				check=1;
			}else {
				check=2;
			}
			Break=1;
		}
	}
	
	static void DiagonalWin() {
		if((board[1][1]==board[2][2]&&board[1][1]==board[3][3]&&board[1][1]!='-')||
			(board[3][1]==board[2][2]&&board[1][3]==board[3][1]&&board[3][1]!='-')){
				if(player=='X') {
					check=1;
				}else {
					check=2;
				}
				Break=1;
		}
	}
	
	static void Draw() {
		if(turn==9)
			Break=1;
	}
	
	static void SwitchTurn() {
		if(player == 'X') {
			player = 'O';
		}else {
			player = 'X';
		}
		turn++;
	}
	
	static void printWin() {
		if(check==1) {
			System.out.println("X Winner");
		}else if(check ==2) {
			System.out.println("O Winner");
		}else { 
			System.out.println("Drew");
		}
	}
	

}
